# Обращаемся к библиотеке
import math

while True:
    mass_g, mass_f, mass_y, mass_x = [], [], [], []
    xmin, xmax, a, x = 0, 0, 0, 0

    while True:
        try:  # Ввод переменных
            a = float(input('Введите значение a:'))
            xmin = int(input("Введите мин значение x:"))
            xmax = int(input("Введите макс значение x:"))
            if xmin >= xmax:
                print('Неправильно введеный границы х')
            else:
                break
        except ValueError:
            print('Ошибка: неправильный формат')

    while True:  # Количество шагов
        try:
            steps = int(input('Кол-во шагов: '))
            if math.isclose(steps, 0.0) or steps < 0 or (xmax - xmin) < steps:
                print('Ошибка: введите еще раз')
            else:
                size = (xmax - xmin) / steps
                break
        except ValueError:
            print('Ошибка: неправильный формат')

    x, step, i = xmin, 0, 1
    string = ''

    # Решение уравнения G и его вывод
    while step <= steps:
        g1 = 6 * (4 * a ** 2 - 12 * a * x + 5 * x ** 2)
        g2 = 9 * a ** 2 + 30 * a * x + 16 * x ** 2
        if math.isclose(g2, 0.0):
            mass_g.append(None)
            mass_x.append(x)
            print('Нет решений, ошибка: знаменатель = 0')
        else:
            g = g1 / g2
            string += str(g)
            mass_g.append(g)
            mass_x.append(x)

    # Решение уравнения F и его вывод
        f = math.sin(28 * a ** 2 - 27 * a * x + 5 * x ** 2)
        if f > 1 or f < -1:
            mass_f.append(None)
            mass_x.append(x)
            print('Нет решений, ошибка: не принадлежит области значений функции')
        else:
            f = math.sin(28 * a ** 2 - 27 * a * x + 5 * x ** 2)
            string += str(f)
            mass_f.append(f)
            mass_x.append(x)

    # Решение уравнения Y и его вывод
        y1 = (21 * a ** 2 + 73 * a * x + 10 * x ** 2 + 1)
        if y1 <= 0:
            mass_y.append(None)
            mass_x.append(x)
            print('Нет решений, ошибка: не принадлежит области допустимых значений функции')
        else:
            y = (math.log(y1)) / (math.log(2))
            string += str(y)
            mass_y.append(y)
            mass_x.append(x)

        print(" ")
        x += size
        step += 1
        i += 1
    set_x = set(mass_x)

    print('Функция G:\n', mass_g)
    print('Функция F:\n', mass_f)
    print('Функция Y:\n', mass_y)

    print(" ")

    print('min G:', min(mass_g, key=lambda i: str(i), default='None'),
            'max G:', max(mass_g, key=lambda i: str(i), default='None'),
          '\nmin F:', min(mass_f, key=lambda i: str(i), default='None'),
            'max F:', max(mass_f, key=lambda i: str(i), default='None'),
          '\nmin Y:', min(mass_y, key=lambda i: str(i), default='None'),
            'max Y:', max(mass_y, key=lambda i: str(i), default='None'))
    print(" ")
    print('Строка значений функций:', string)

    while True:
        try:
            num = str(input('Что искать?: '))
            print('Количество совпадений: ', string.count(num))
            break
        except ValueError:
            print('Цифры пиши')

    # Выход из цикла по требованию пользователя
    while True:
        try:
            end = int(input("Выйти из цикла? ДА[1]/НЕТ[2]: "))
            if end == 1:
                print('Выхожу из программы...')
                exit(0)
            elif end == 2:
                print("Заново производим цикл")
                break
            else:
                print("Ошибка: нет такого варианта выбора, выберите 0 или 1")
        except ValueError:
            print("Ошибка: неправильный формат")
